r-cran-maptools (1:1.1-8+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 24 Jul 2023 19:12:46 +0200

r-cran-maptools (1:1.1-7+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 24 Jun 2023 23:39:15 +0200

r-cran-maptools (1:1.1-6+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 20 Dec 2022 14:40:55 +0100

r-cran-maptools (1:1.1-5+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 25 Oct 2022 09:51:53 +0200

r-cran-maptools (1:1.1-4+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 17 May 2022 11:59:52 +0200

r-cran-maptools (1:1.1-3+dfsg-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 11 Mar 2022 07:10:40 +0100

r-cran-maptools (1:1.1-2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 15 Sep 2021 17:28:51 +0200

r-cran-maptools (1:1.0-2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Refer to specific version of license GPL-2+.

 -- Andreas Tille <tille@debian.org>  Wed, 02 Sep 2020 15:25:21 +0200

r-cran-maptools (1:1.0-1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Sun, 24 May 2020 09:41:25 +0200

r-cran-maptools (1:0.9-9+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Sat, 07 Dec 2019 07:16:16 +0100

r-cran-maptools (1:0.9-8+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Set upstream metadata fields: Archive.

 -- Andreas Tille <tille@debian.org>  Mon, 07 Oct 2019 11:27:24 +0200

r-cran-maptools (1:0.9-5+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Remove trailing whitespace in debian/copyright
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Mon, 15 Jul 2019 12:09:00 +0200

r-cran-maptools (1:0.9-4+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Fri, 28 Sep 2018 21:00:33 +0200

r-cran-maptools (1:0.9-3+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.5

 -- Andreas Tille <tille@debian.org>  Thu, 02 Aug 2018 03:05:04 +0200

r-cran-maptools (1:0.9-2+dfsg-2) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Mon, 04 Jun 2018 13:18:06 +0200

r-cran-maptools (1:0.9-2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.0 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Thu, 07 Sep 2017 11:35:12 +0200

r-cran-maptools (1:0.8-41+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Move packaging to Git
  * TODO: Add explicit dependency: r-cran-rgeos (see README.source)

 -- Andreas Tille <tille@debian.org>  Sun, 15 Jan 2017 20:07:13 +0100

r-cran-maptools (1:0.8-40+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Wed, 30 Nov 2016 09:08:03 +0100

r-cran-maptools (1:0.8-39+dfsg-1) unstable; urgency=medium

  * New upstream version
  * xz compression in d/watch
  * Convert to dh-r
  * Canonical homepage for CRAN
  * cme fix dpkg-copyright

 -- Andreas Tille <tille@debian.org>  Fri, 04 Nov 2016 14:44:18 +0100

r-cran-maptools (1:0.8-30-1) unstable; urgency=medium

  * New upstream version
  * debian/copyright: exclude files with unclear license
  * Upload to Debian main since no non-free parts are remaining
  * debian/README.source: document binary data files
  * debian/README.Debian: Hint about removed data files

 -- Andreas Tille <tille@debian.org>  Mon, 23 Jun 2014 14:22:43 +0200

r-cran-maptools (1:0.8-23-1) unstable; urgency=low

  * New upstream version
  * debian/source/format: 3.0 (quilt)
  * debian/control:
     - Standards-Version: 3.9.4 (no changes needed)
     - DM-Upload-Allowed removed
     - debhelper 9 (also debian/compat)
     - normalised using `cme fix dpkg-control`
  * Separate information about testing from README.Debian
  * debian/copyright: Use `cme fix dpkg-copyright` to make it DEP5
    compatible
  * debian/rules: Drop unneeded code to create R:Depends

 -- Andreas Tille <tille@debian.org>  Thu, 16 May 2013 17:35:43 +0200

r-cran-maptools (1:0.7-38-1) unstable; urgency=low

  * New upstream version
  * Standards-Version: 3.9.1 (no changes needed)
  * debian/rules: Remove extra license file in /usr/lib, content is
    just in debian/copyright

 -- Andreas Tille <tille@debian.org>  Wed, 24 Nov 2010 08:37:20 +0100

r-cran-maptools (1:0.7-34-2) unstable; urgency=low

  * debian/rules: Make sure whitespace is really a tab to get
    R:Depends substvar working

 -- Andreas Tille <tille@debian.org>  Mon, 31 May 2010 13:54:16 +0200

r-cran-maptools (1:0.7-34-1) unstable; urgency=low

  * New upstream version
  * Depend on a version equal or superior than the R upstream release that
    was used to build this package, using a R:Depends substvar
    (debian/control, debian/rules).
  * debian/copyright: More generic value for Upstream-Source field
  * debian/README.Debian: explain how to test this package

 -- Andreas Tille <tille@debian.org>  Thu, 20 May 2010 15:22:15 +0200

r-cran-maptools (1:0.7-29-1) unstable; urgency=low

  * New upstream version
  * Use upstream version scheme with dash '-' which requires epoch
    in changelog
  * debian/watch: Removed unneeded version mangling
  * debian/get-orig-source: removed because unneeded (uscan is
    perfectly sufficient)
  * Standards-Version: 3.8.4 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Wed, 10 Feb 2010 15:02:12 +0100

r-cran-maptools (0.7.26-1) unstable; urgency=low

  * New upstream version
  * Further note to the copyright given in mail to the debian-science
    list by the copyright holders of the critical data files which
    make the package non-free
  * Standards-Version: 3.8.3 (no changes needed)
  * Changed Section from non-free/science to non-free/gnu-r

 -- Andreas Tille <tille@debian.org>  Fri, 30 Oct 2009 11:27:51 +0100

r-cran-maptools (0.7.20-1) unstable; urgency=low

  * Initial release (Closes: #515646).
  * Many thanks to Charles Plessy who did a really great job
    to review the copyright

 -- Andreas Tille <tille@debian.org>  Fri, 13 Mar 2009 09:36:18 +0100
